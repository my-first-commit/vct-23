# Git Workshop

This repo is made for Git Workshop at Vimala College, Thrissur.

## Getting started

To make it easy for you to get started, here's a list of recommended next steps.

### Clone this repo

```
git clone https://gitlab.com/my-first-commit/vct-23.git
```

### Create a branch and add your files

Create a branch for your favorite Computer Science subject and add a file about the same.

Useful commands (Replace text in angular brackets with proper values):
```
git branch <branch_name>
touch <filename>
git add <filename>
git commit -m 'My first commit'
git push --set-upstream origin <branch_name>
```

## License

[GPL-3.0-or-later](LICENSE)
